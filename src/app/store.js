import { combineReducers } from "redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import weatherReducer from "../reducers/weather.reducer";

//root chứa các task reducer 
const rootReducer = combineReducers({
    //gọi các task
    weatherReducer
});

//tạo store
const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;