import { Container, Grid, Typography } from "@mui/material";

import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getWeatherToday } from "../action/weather.action";

import iconWeather from "../asset/images/02d.svg";
import InputCity from "./InputCity";
import WeatherToday from "./WeatherToday";

const WeatherComponent = () => {
    const dispatch = useDispatch();

    const { city, weatherToday } = useSelector((data) => {
        return data.weatherReducer
    });

    useEffect(() => {
        if (city != "") {
            dispatch(getWeatherToday(city))
        }
    }, [city]);

    return (
        <Container>
            {
                weatherToday
                    ?
                    <WeatherToday weatherData={weatherToday} />
                    :
                    <Grid container
                        sx={{
                            borderRadius: "30px",
                            background: "hsla(0,0%,100%,.2)",
                            boxShadow: "0 0 10px silver",
                        }}
                        p={10}
                        m={5}
                    >
                        <Grid item xs={12}>
                            <Typography
                                variant="h3"
                                component="div"
                                sx={{ color: "#365a7a" }}
                                alignSelf="center"
                                alignItems="center"
                                textAlign="center"
                            >
                                <Typography component="img" src={iconWeather} mr={1} mt={2} minWidth="100px" />
                                Weather Forecast
                            </Typography>
                        </Grid>
                        <InputCity />
                    </Grid>
            }
        </Container >
    )
}

export default WeatherComponent;