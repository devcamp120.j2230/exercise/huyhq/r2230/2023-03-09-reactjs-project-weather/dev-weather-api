import { Grid, TextField } from "@mui/material"
import { useDispatch } from "react-redux";
import { GET_CITY } from "../constants/weather.constant";

const InputCity = () => {
    const dispatch = useDispatch();

    const onCityConfirm = (e) => {
        //Kiểm tra "Enter" có được nhấn
        if (e.key == "Enter") {
            dispatch({
                type: GET_CITY,
                city: e.target.value
            })
        }
    };

    return (
        <Grid item xs={12} justifyContent="center" textAlign="center" margin={5}>
            <TextField
                size="medium"
                placeholder="Enter a City ..."
                sx={{
                    borderRadius: "20px",
                    width: "600px",
                    margin: "20px 0"
                }}
                onKeyDown={onCityConfirm} />
        </Grid>
    )
}

export default InputCity