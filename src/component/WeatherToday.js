import { Container, Grid, Typography } from "@mui/material";
import InputCity from "./InputCity";

const WeatherToday = (prop) => {
    const cityName = prop.weatherData.name;
    const temp = prop.weatherData.main.temp;
    const status = prop.weatherData.weather[0].main;
    const icon = prop.weatherData.weather[0].icon;

    return (
        <Container>
            <Grid container p={1} m={5}>
                <InputCity />
            </Grid>
            <Grid
                container
                sx={{
                    borderRadius: "30px",
                    background: "hsla(0,0%,100%,.2)",
                    boxShadow: "0 0 10px silver",
                }}
                p={10}
                m={5}
            >
                <Grid item xs={6} justifyContent="center" textAlign="center">
                    <Typography component="img" src={require(`../asset/images/${icon}.svg`)} mr={1} mt={2} minWidth="200px" />
                </Grid>
                <Grid item xs={6} py={10} justifyContent="center" textAlign="left" alignItems="center" alignContent="center">
                    <Typography component="div">Today</Typography>
                    <Typography variant="h3" component="div">{cityName}</Typography>
                    <Typography component="div">Temperature: {temp}<sup> o</sup>C</Typography>
                    <Typography component="div">{status}</Typography>
                </Grid>
            </Grid>
            <Grid container position="relative" maxWidth="80%" top={-120} sx={{margin: "0 auto"}}>
                <Grid
                    item xs={3}
                    justifyContent="center"
                    textAlign="center"
                    p={2}
                >
                    <Typography
                        component="div"
                        sx={{
                            borderRadius: "50px",
                            background: "hsla(0,0%,100%,.2)",
                            boxShadow: "0 0 10px silver",
                        }}
                    >
                        <Typography variant="h5" component="div">Tomorrow</Typography>
                        <Typography component="img" src={require(`../asset/images/${icon}.svg`)} minWidth="100px" />
                        <Typography component="div">{temp}<sup> o</sup>C</Typography>
                    </Typography>
                </Grid>
                <Grid
                    item xs={3}
                    justifyContent="center"
                    textAlign="center"
                    p={2}
                >
                    <Typography
                        component="div"
                        sx={{
                            borderRadius: "50px",
                            background: "hsla(0,0%,100%,.2)",
                            boxShadow: "0 0 10px silver",
                        }}
                    >
                        <Typography variant="h5" component="div">Tomorrow</Typography>
                        <Typography component="img" src={require(`../asset/images/${icon}.svg`)} minWidth="100px" />
                        <Typography component="div">{temp}<sup> o</sup>C</Typography>
                    </Typography>
                </Grid>
                <Grid
                    item xs={3}
                    justifyContent="center"
                    textAlign="center"
                    p={2}
                >
                    <Typography
                        component="div"
                        sx={{
                            borderRadius: "50px",
                            background: "hsla(0,0%,100%,.2)",
                            boxShadow: "0 0 10px silver",
                        }}
                    >
                        <Typography variant="h5" component="div">Tomorrow</Typography>
                        <Typography component="img" src={require(`../asset/images/${icon}.svg`)} minWidth="100px" />
                        <Typography component="div">{temp}<sup> o</sup>C</Typography>
                    </Typography>
                </Grid>
                <Grid
                    item xs={3}
                    justifyContent="center"
                    textAlign="center"
                    p={2}
                >
                    <Typography
                        component="div"
                        sx={{
                            borderRadius: "50px",
                            background: "hsla(0,0%,100%,.2)",
                            boxShadow: "0 0 10px silver",
                        }}
                    >
                        <Typography variant="h5" component="div">Tomorrow</Typography>
                        <Typography component="img" src={require(`../asset/images/${icon}.svg`)} minWidth="100px" />
                        <Typography component="div">{temp}<sup> o</sup>C</Typography>
                    </Typography>
                </Grid>
            </Grid>
        </Container >
    )
}

export default WeatherToday;