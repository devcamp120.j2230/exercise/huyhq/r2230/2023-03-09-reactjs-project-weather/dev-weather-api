import WeatherComponent from "./component/WeatherComponent";
import "./App.css"

function App() {
  return (
    <div>
      <WeatherComponent />
    </div>
  );
}

export default App;
