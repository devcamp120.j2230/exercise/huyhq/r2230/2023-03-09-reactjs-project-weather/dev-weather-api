import axios from "axios";
import { useSelector } from "react-redux";
import { DATA_FETCH_ERROR, DATA_FETCH_PENDING, DATA_FETCH_SUCCESS } from "../constants/weather.constant";

// const { city } = useSelector((data) => {
//     return data.weatherReducer
// });
const url = "https://api.openweathermap.org/data/2.5/weather";
const keyApi = "1311cc5e2e6fa434135721390e3f7b52";
const unit = "metric"
const axiosCall = async (url, body) => {
    const response = await axios(url, body);
    return response.data;
};

export const getWeatherToday = (city) => {
    return async (dispatch) => {
        await dispatch({
            type: DATA_FETCH_PENDING
        })

        axiosCall(url + `?q=${city}&units=${unit}&appid=${keyApi}`)
            .then(result => {
                return dispatch({
                    type: DATA_FETCH_SUCCESS,
                    data: result
                })
            })
            .catch(error => {
                return dispatch({
                    type: DATA_FETCH_ERROR,
                    error: error
                })
            });
    }
}