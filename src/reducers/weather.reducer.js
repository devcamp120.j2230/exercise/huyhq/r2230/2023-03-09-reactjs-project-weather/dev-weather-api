import { DATA_FETCH_ERROR, DATA_FETCH_PENDING, DATA_FETCH_SUCCESS, GET_CITY } from "../constants/weather.constant";

const taskState = {
    weatherToday: "",
    city: "",
    pending: false
};

const weatherReducer = (state = taskState, action) => {
    switch (action.type) {
        case DATA_FETCH_PENDING:
            state.pending = true;
            break;
        case DATA_FETCH_SUCCESS:
            state.pending = false;
            state.weatherToday = action.data;
            console.log(action.data);
            break;
        case DATA_FETCH_ERROR:
            break;
        case GET_CITY:
            state.city = action.city;
            break;
        default:
            break;
    }
    return { ...state };
};

export default weatherReducer;